import Ember from "ember";

export default Ember.Route.extend({
    model: function(params) {
        console.log("IN INCLUDE" + params.index);
        return {index: params.index};
    },
    renderTemplate: function(model, options){
      this.render('hats/include',{
        'into':'hats',
        'outlet': options.index,
        'model': options.index
      });
    },
    actions: {
        willTransition: function() {
            this.disconnectOutlet(this.context.index);
        }
    }
});
