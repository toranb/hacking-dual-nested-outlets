import Ember from "ember";

export default Ember.Route.extend({
    model: function(params) {
        console.log("IN DETAIL" + params.index);
        //return shared stuff ?
        return {index: params.index};
    },
    renderTemplate: function(model, options){
      this.render('hats/detail',{
        'into':'hats',
        'outlet': options.index,
        'model': options.index
      });
    },
    actions: {
        willTransition: function() {
            this.disconnectOutlet(this.context.index);
        }
    }
});
